#!/bin/bash
CURR_DIR=$(cd "$(dirname "$0")"; pwd)
PROPERTY_FILE=$CURR_DIR"/ucare_payor_env.properties"

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`
   cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}

payorUser=$(getProperty payorUser)
payorHosts=$(getProperty payorHosts)
weblogicMappingFile=$(getProperty weblogicDomainDir)/data/defaultpathnamemapping.txt

export IFS=","
for payorHost in $payorHosts; do


echo "Replacing defaultpathnamemapping on $payorHost"
ssh -q  ${payorUser}@${payorHost} "echo -n >  ${weblogicMappingFile}"
ssh -q  ${payorUser}@${payorHost} "cat > ${weblogicMappingFile} << EOF
COBPolicy=COB {0} 
COBPolicy.caseStatus=COB Status {0} 
Membership.individual.primaryName.lastName=MemberLastName 
Membership.individual.primaryName.firstName=MemberFirstName 
Subscription.identifiedAccount=AccountChange 
EOF"

done

exit 0

