#!/bin/bash
set -e
LETTER_TYPE=$1
CCPE_INPUT_PARAM=$2

source ~/.bashrc
script_name=$0
script_full_path=$(dirname "$0")
cd $script_full_path
echo $PWD
START_TIME=`date "+%Y%m%d-%H%M%S"`
echo "CCPE processing started at "$START_TIME
echo "LETTER_TYPE: "$LETTER_TYPE" and CCPE_INPUT_PARAM: "$CCPE_INPUT_PARAM
java -jar ccpe-*.jar $LETTER_TYPE $CCPE_INPUT_PARAM
END_TIME=`date "+%Y%m%d-%H%M%S"`
echo "CCPE processing comepleted at "$END_TIME
