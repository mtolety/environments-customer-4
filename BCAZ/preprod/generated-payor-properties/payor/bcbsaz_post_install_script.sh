#!/bin/bash

payorUser=weblogic
payorHost=172.29.28.183
payorClaimServerHosts=172.29.28.184,172.29.28.185,172.29.28.186,172.29.28.187,172.29.28.188,172.29.28.245,172.29.28.246,172.29.28.247
weblogicDomainDir=/home/weblogic/oracle/wls_12.2.1.2.0/user_projects/domains/preprod
payorEnvironmentDir=/home/weblogic/environments/preprod

payorWLXmlFolder=${weblogicDomainDir}/xml
customFilesFolder=${payorEnvironmentDir}/custom-files/xml


echo "Copying MVAConfig.xml from custom-files.txt to the admin Weblogic Server from bcbsaz_post_install_script."
ssh -q  ${payorUser}@${payorHost} "cp ${customFilesFolder}/MVAConfig.xml ${payorWLXmlFolder}"

export IFS=","
for payorClaimServerHost in $payorClaimServerHosts; do
echo "Copying MVAConfig.xml from admin WL server to claim server ${payorClaimServerHost} from bcbsaz_post_install_script."
ssh -q ${payorUser}@${payorHost} "scp ${payorWLXmlFolder}/MVAConfig.xml ${payorUser}@${payorClaimServerHost}:${payorWLXmlFolder}"

done

exit 0
